import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

public class main {

    public static void SocketConnection(String url, String filename) {
        try {
            try (Socket socket = new Socket(url, 80);
                 BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                 BufferedWriter fileWriter = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filename)))) {

                PrintWriter printWriter = new PrintWriter(socket.getOutputStream());
                printWriter.println("GET / HTTP/1.1");
                printWriter.println("");
                printWriter.flush();
                writeToFileSocket(reader, fileWriter);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        final String FILE_NAME = "fileData.html";
        Scanner in = new Scanner(System.in);
        System.out.print("Input an url: ");
        String url = in.nextLine();
        //SocketConnection(url, FILE_NAME);
        urlConnection(url, FILE_NAME);

    }

    public static void urlConnection(String url, String filename) throws IOException {
        URLConnection connection = new URL("https://" + url).openConnection();
        BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        BufferedWriter fileWriter = new BufferedWriter(new FileWriter(filename));
        System.out.println(connection.getHeaderFields());
        writeToFileUrlConnection(reader, fileWriter);
        fileWriter.close();
        reader.close();
    }

    public static void writeToFileSocket(BufferedReader reader, BufferedWriter fileWriter) throws IOException {
        String line;
        line = reader.readLine();
        boolean headerOver = false;
        while (line != null) {
            if (line.isEmpty()) {
                headerOver = true;
            }
            if (headerOver) {
                fileWriter.write(line);
            } else {
                System.out.println(line);
            }
            if (line.contains("</html>")) {
                break;
            }
            line = reader.readLine();
        }
    }

    public static void writeToFileUrlConnection(BufferedReader reader, BufferedWriter fileWriter) throws IOException {
        String line;
        line = reader.readLine();
        while (line != null) {
            fileWriter.write(line);
            line = reader.readLine();
        }
    }

}
